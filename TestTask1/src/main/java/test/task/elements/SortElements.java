package test.task.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SortElements extends BaseElements {

    @FindBy(partialLinkText = "по цене")
    private WebElement sortByPpriceElement;

    public SortElements(WebDriver driver) {
        super(driver);
    }

    public void sortByPrice(){
        sortByPpriceElement.click();
    }


}
